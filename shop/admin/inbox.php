﻿<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php
    $filePath = realpath(dirname(__FILE__));
    include_once ($filePath.'/../classes/Cart.php');
    $cart = new Cart;
    $fm = new Format();
?>
<?php
    if (isset($_GET['shiftid'])){
        $id = $_GET['shiftid'];
        $price = $_GET['price'];
        $shift = $cart->productShifted($id, $price);
    }
    if (isset($_GET['delProId'])){
        $id = $_GET['delProId'];
        $price = $_GET['price'];
        $delProId = $cart->delproductShifted($id, $price);
    }
?>


        <div class="grid_10">
            <div class="box round first grid">
                <h2>Inbox</h2>
                <?php
                    if (isset($shift)){
                        echo $shift;
                    }
                    if (isset($delProId)){
                        echo $delProId;
                    }
                ?>
                <div class="block">        
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>ID</th>
							<th>Order Time </th>
							<th>Product</th>
							<th>Quantity</th>
							<th>Price</th>
							<th>Cust. ID</th>
							<th>Address</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
                    <?php
                        $getOrder = $cart->getAllOrderPro();
                        if ($getOrder){
                            while($result = $getOrder->fetch_assoc()){ ?>
						<tr class="odd gradeX">
							<td><?php echo $result['id'];?></td>
							<td><?php echo $fm->formatDate($result['date']);?></td>
							<td><?php echo $result['productName'];?></td>
							<td><?php echo $result['quantity'];?></td>
							<td>$<?php echo $result['price'];?></td>
							<td><?php echo $result['customerid'];?></td>
							<td><a href="customer.php?customerid=<?php echo $result['customerid'];?>">View Details</a></td>
                            <?php
                            if ($result['status'] == '0') { ?>
                                <td><a href="?shiftid=<?php echo $result['customerid'] ;?>&price=<?php echo $result['price'] ;?>">Shifted</a></td>
                            <?php } elseif($result['status'] == '1') { ?>
                                <td>Pending</td>
                            <?php }else{ ?>
                                <td><a href="?delProId=<?php echo $result['customerid'] ;?>&price=<?php echo $result['price'] ;?>">Remove</a></td>
                           <?php } ?>
						</tr>
                    <?php }} ?>
					</tbody>
				</table>
               </div>
            </div>
        </div>
<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();

        $('.datatable').dataTable();
        setSidebarHeight();
    });
</script>
<?php include 'inc/footer.php';?>
