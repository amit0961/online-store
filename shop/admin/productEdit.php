<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/category.php'; ?>
<?php include '../classes/Product.php'; ?>
<?php include '../classes/Brand.php'; ?>
<?php
if (!isset($_GET['productId']) || $_GET['productId']==null){
    echo "<script>window.location='productList.php'; </script>";
}else{
    $id= preg_replace('/[^-a-zA-Z0-9_]/','',$_GET['productId']) ;
}

$product = new Product();
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {
    $updateProduct = $product->productUpdate($_POST, $_FILES, $id);
}
?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Update  Product</h2>

            <div class="block copyblock">
                <?php
                if (isset($updateProduct)){
                    echo $updateProduct;
                }
                ?>
                <?php
                $getproduct = $product->getProductByID($id);
                if ($getproduct){
                    while($value = $getproduct->fetch_assoc()){
                        ?>
                        <form action="" method="post">
                            <table class="form">
                                <tr>
                                    <td>
                                        <label>Product Name</label>
                                    </td>
                                    <td>
                                        <input type="text" name="productName" value="<?php echo $value['productName'] ;?> " class="medium" />
                                    </td>
                                </tr>
                                <tr>
                                <tr>
                                    <td>
                                        <label>Category</label>
                                    </td>
                                    <td>
                                        <select id="select" name="catID">
                                            <option>Select Category</option>
                                            <?php
                                            $category = new Category();
                                            $getCategory = $category->getallCategory();
                                            if ($getCategory){
                                                while ($result = $getCategory->fetch_assoc()){
                                                    ?>
                                                    <option
                                                        <?php
                                                        if ($value['catID'] == $result['catID']){ ?>
                                                            selected = "Selected"
                                                      <?php  }
                                                        ?>
                                                        value="<?php echo $result['catID'];?>"><?php echo $result['catName'];?>
                                                    </option>
                                                <?php }} ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Brand</label>
                                    </td>
                                    <td>
                                        <select id="select" name="brandID">
                                            <option>Select Brand</option>
                                            <?php
                                            $brand = new Brand();
                                            $getBrand = $brand->getallBrand();
                                            if ($getBrand){
                                                while ($result = $getBrand->fetch_assoc()){
                                                    ?>
                                                    <option
                                                        <?php
                                                        if ($value['brandID'] == $result['brandID']){ ?>
                                                            selected = "Selected"
                                                        <?php  }
                                                        ?>
                                                        value="<?php echo $result['brandID']; ?>"><?php echo $result['brandName'];?>
                                                    </option>
                                                <?php }} ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="vertical-align: top; padding-top: 9px;">
                                        <label>Description</label>
                                    </td>
                                    <td>
                                        <textarea name="body" class="tinymce" ><?php echo $value['body'] ;?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Price</label>
                                    </td>
                                    <td>
                                        <input type="text" name="price" value="<?php echo $value['price'] ;?>" class="medium" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label>Upload Image</label>
                                    </td>
                                    <td>
                                        <img src="<?php echo $value['image'];?>" height="80px" width="200px" alt="">
                                        <br>
                                        <input type="file" name="image" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label>Product Type</label>
                                    </td>
                                    <td>
                                        <select id="select" name="type">
                                            <option>Select Type</option>
                                            <?php
                                            if ($value['type']==0){ ?>
                                                <option selected="selected" value="0">Featured</option>
                                                <option value="1">General</option>
                                                <?php }else{ ?>
                                                <option selected="selected" value="1">General</option>
                                            <option value="0">Featured</option>

                                                <?php }?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="submit" name="submit" Value="Update" />
                                    </td>
                                </tr>
                            </table>
                        </form>
                    <?php }} ?>
            </div>
        </div>
    </div>
<?php include 'inc/footer.php';?>