<?php
    $filePath = realpath(dirname(__FILE__));
    include_once ($filePath.'/../library/database.php');
    include_once ($filePath.'/../helpers/format.php');
?>
<?php
class Brand
{
    private $db;
    private $fm;
    public function __construct()
    {
        $this->db = new Database();
        $this->fm = new Format();
    }
    public function brandInsert($brandName){
        $brandName = $this->fm->validation($brandName);
        $brandName = mysqli_real_escape_string($this->db->link,$brandName);
        if (empty($brandName)){
            $msg = "<span class='error'>Brand field must be required </span>";
            return $msg;
        }else{
            $query = "INSERT INTO table_brand(brandName) VALUES ('$brandName')";
            $brandInsert = $this->db->insert($query);
            if ($brandInsert){
                $msg = "<span class='success'>Brand Inserted Successfully... </span>";
                return $msg;
            }else{
                $msg = "<span class='error'>Opps...! Brand not Inserted ... </span>";
                return $msg;
            }
        }
    }
    public function getallBrand(){
        $query = "SELECT * FROM table_brand ORDER BY brandID DESC ";
        $result = $this->db->select($query);
        return $result;
    }
    public function getBrandByID($id){
        $query = "SELECT * FROM table_brand WHERE brandID ='$id' ";
        $result = $this->db->select($query);
        return $result;
    }
    public function brandUpdate($brandName, $id){
        $brandName = $this->fm->validation($brandName);
        $brandName = mysqli_real_escape_string($this->db->link,$brandName);
        $id = mysqli_real_escape_string($this->db->link,$id);
        if (empty($brandName)){
            $msg = "<span class='error'>Brand field must be required </span>";
            return $msg;
        }else{
            $query = "UPDATE table_brand SET brandName ='$brandName' WHERE brandID='$id'";
            $brandUpdated = $this->db->update($query);
            if ($brandUpdated){
                $msg = "<span class='success'>Brand Updated Successfully... </span>";
                return $msg;
            }else{
                $msg = "<span class='error'>Opps...! Brand not Updated ... </span>";
                return $msg;
            }
        }
    }
    public function delBrandByID($id){
        $query = "DELETE FROM table_brand WHERE brandID ='$id'";
        $brandDeleted = $this->db->delete($query);
        if ($brandDeleted){
            $msg = "<span class='success'>Brand Deleted Successfully... </span>";
            return $msg;
        }else{
            $msg = "<span class='error'>Opps...! Brand not Delete ... </span>";
            return $msg;
        }
    }

}
?>