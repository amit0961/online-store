<?php
$filePath = realpath(dirname(__FILE__));
include_once ($filePath.'/../library/database.php');
include_once ($filePath.'/../helpers/format.php');
?>

<?php
class Cart
{
    private $db;
    private $fm;
    public function __construct()
    {
        $this->db = new Database();
        $this->fm = new Format();
    }
    public function addToCart($quantity, $id){
        $quantity = $this->fm->validation($quantity);
        $quantity = mysqli_real_escape_string($this->db->link,$quantity);
        $productId = mysqli_real_escape_string($this->db->link,$id);
        $sId = session_id() ;

        $query = "SELECT * FROM table_product WHERE productId= '$productId'";
        $result = $this->db->select($query)->fetch_assoc();

        $productName = $result['productName'];
        $price = $result['price'];
        $image = $result['image'];
        $checkQuery =  "SELECT * FROM table_cart WHERE productId= '$productId' AND sId='$sId'";
        $getPro = $this->db->select($checkQuery);
        if ($getPro){
            $msg = "Opps...! Product already Added .";
            return $msg;
        }else{
            $query = "INSERT INTO table_cart (sId,productId,productName,price,quantity,image) 
                VALUES('$sId','$productId','$productName','$price','$quantity','$image')";
            $inserted_rows = $this->db->insert($query);
            if ($inserted_rows) {
                header("Location:cart.php");
            }else {
                header("Location:404.php");
            }
        }




    }
    public function getCartProduct(){
        $sId = session_id();
        $query = "SELECT * FROM table_cart WHERE   sId = '$sId'";
        $result = $this->db->select($query);
        return $result;

    }
    public function updateCartQuantity($cartId, $quantity){
        $cartId = mysqli_real_escape_string($this->db->link,$cartId);
        $quantity = mysqli_real_escape_string($this->db->link,$quantity);
        $query = "UPDATE table_cart 
                    SET 
                    quantity ='$quantity' 
                    WHERE 
                    cartId='$cartId'";
        $cartUpdated = $this->db->update($query);
        if ($cartUpdated){
            header("Location:cart.php");
        }else{
            $msg = "<span class='error'>Opps...! Cart not Updated ... </span>";
            return $msg;
        }
    }
    public function delProByCartId($delCartId){
        $delCartId = mysqli_real_escape_string($this->db->link,$delCartId);
        $query = "DELETE FROM table_cart WHERE cartId ='$delCartId'";
        $cartDeleted = $this->db->delete($query);
        if ($cartDeleted){
            echo "<script> window.location= 'cart.php' ; </script>";
        }else{
            $msg = "<span class='error'>Opps...! Cart not Delete ... </span>";
            return $msg;
        }
    }
    public function checkCart(){
        $sId = session_id();
        $query = "SELECT * FROM table_cart WHERE   sId = '$sId'";
        $result = $this->db->select($query);
        return $result;
    }
    public function delCustomerCart(){
        $sId = session_id();
        $query = "DELETE  FROM table_cart WHERE   sId = '$sId'";
        $this->db->delete($query);
    }
    public function orderProduct($customerid){
        $sId = session_id();
        $query = "SELECT * FROM table_cart WHERE   sId = '$sId'";
        $getPro = $this->db->select($query);
        if ($getPro){
            while ($result = $getPro->fetch_assoc()){
                $productId= $result['productId'];
                $productName= $result['productName'];
                $quantity= $result['quantity'];
                $price= $result['price'] * $quantity;
                $image= $result['image'];
                $query = "INSERT INTO table_order (customerid,productId,productName,quantity,price,image) 
                VALUES('$customerid','$productId','$productName','$quantity','$price','$image')";
                $inserted_rows = $this->db->insert($query);
            }

        }
    }
    public function payableamount($customerid){
        $query = "SELECT price FROM table_order WHERE   customerid = '$customerid' AND date = now()";
        $result = $this->db->select($query);
        return $result;
    }
    public function getOrderProduct($customerid){
        $query = "SELECT * FROM table_order WHERE   customerid = '$customerid' ORDER BY productId DESC ";
        $result = $this->db->select($query);
        return $result;
    }
    public function checkOder($customerid){

        $query = "SELECT * FROM table_order WHERE   customerid = '$customerid'";
        $result = $this->db->select($query);
        return $result;
    }
    public function getAllOrderPro(){
        $query = "SELECT * FROM table_order ORDER By date";
        $result = $this->db->select($query);
        return $result;
    }
    public function productShifted($id, $price){
        $id = mysqli_real_escape_string($this->db->link,$id);
        $price = mysqli_real_escape_string($this->db->link,$price);
        $query = "UPDATE table_order SET status ='1' WHERE customerid='$id' AND price='$price'";
        $catUpdated = $this->db->update($query);
        if ($catUpdated){
            $msg = "<span class='success'> Updated Successfully... </span>";
            return $msg;
        }else{
            $msg = "<span class='error'>Opps...! not Updated ... </span>";
            return $msg;
        }
    }
    public function delproductShifted($id, $price){
        $id = mysqli_real_escape_string($this->db->link,$id);
        $price = mysqli_real_escape_string($this->db->link,$price);
        $query = "DELETE FROM table_order WHERE customerid='$id' AND price='$price'";
        $orderDeleted = $this->db->delete($query);
        if ($orderDeleted){
            $msg = "<span class='success'> Deleted Successfully... </span>";
            return $msg;
        }else{
            $msg = "<span class='error'>Opps...! not Deleteted ... </span>";
            return $msg;
        }
    }
    public function productShiftConfirm($id, $price){
        $id = mysqli_real_escape_string($this->db->link,$id);
        $price = mysqli_real_escape_string($this->db->link,$price);
        $query = "UPDATE table_order SET status ='2' WHERE customerid='$id' AND price='$price'";
        $catUpdated = $this->db->update($query);
        if ($catUpdated){
            $msg = "<span class='success'> Updated Successfully... </span>";
            return $msg;
        }else{
            $msg = "<span class='error'>Opps...! not Updated ... </span>";
            return $msg;
        }
    }






















}
?>