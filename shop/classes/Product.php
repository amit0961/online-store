<?php
$filePath = realpath(dirname(__FILE__));
include_once($filePath . '/../library/database.php');
include_once($filePath . '/../helpers/format.php');
?>

<?php

class Product
{
    private $db;
    private $fm;

    public function __construct()
    {
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function productInsert($data, $file)
    {
        $productName = mysqli_real_escape_string($this->db->link, $data['productName']);
        $catID = mysqli_real_escape_string($this->db->link, $data['catID']);
        $brandID = mysqli_real_escape_string($this->db->link, $data['brandID']);
        $body = mysqli_real_escape_string($this->db->link, $data['body']);
        $price = mysqli_real_escape_string($this->db->link, $data['price']);
        $type = mysqli_real_escape_string($this->db->link, $data['type']);
//for image
        $permited = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $file['image']['name'];
        $file_size = $file['image']['size'];
        $file_temp = $file['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_image = "uploads/" . $unique_image;
        //for image
        if ($productName == "" || $catID == "" || $brandID == "" || $body == "" || $price == "" || $type == "") {
            $msg = "<span class='error'> Fields must be required.</span>";
            return $msg;
        } elseif ($file_size > 1048567) {
            echo "<span class='error'>Image Size should be less then 1MB!</span>";
        } elseif (in_array($file_ext, $permited) === false) {
            echo "<span class='error'>You can upload only:- " . implode(', ', $permited) . "</span>";
        } else {
            move_uploaded_file($file_temp, $uploaded_image);
            $query = "INSERT INTO table_product (productName,catID,brandID,body,price,type,image) VALUES('$productName','$catID','$brandID','$body','$price','$type','$uploaded_image')";
            $inserted_rows = $this->db->insert($query);
            if ($inserted_rows) {
                echo "<span class='success'>Product Inserted Successfully.</span>";
            } else {
                echo "<span class='error'>Product Not Inserted !</span>";
            }
        }

    }


    public function getAllProduct()
    {

        $query = "SELECT table_product.*, table_category.catName, table_brand.brandName
       FROM table_product
       INNER JOIN table_category
        ON table_product.catID = table_category.catID
        INNER JOIN table_brand
        ON table_product.brandID = table_brand.brandID
        ORDER BY table_product.productId DESC ";
        $result = $this->db->select($query);
        return $result;


//        $query = "SELECT p.*, c.catName, b.brandName
//        FROM table_product as p, table_brand as b , table_category as c
//        WHERE p.catID = c.catID and p.brandID = b.brandID
//        ORDER BY p.productId DESC ";

    }

    public function getProductByID($id)
    {
        $query = "SELECT * FROM table_product WHERE productId ='$id' ";
        $result = $this->db->select($query);
        return $result;
    }

    public function productUpdate($data, $file, $id)
    {
        $productName = mysqli_real_escape_string($this->db->link, $data['productName']);
        $catID = mysqli_real_escape_string($this->db->link, $data['catID']);
        $brandID = mysqli_real_escape_string($this->db->link, $data['brandID']);
        $body = mysqli_real_escape_string($this->db->link, $data['body']);
        $price = mysqli_real_escape_string($this->db->link, $data['price']);
        $type = mysqli_real_escape_string($this->db->link, $data['type']);
//for image
        $permited = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $file['image']['name'];
        $file_size = $file['image']['size'];
        $file_temp = $file['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_image = "uploads/" . $unique_image;
        //for image
        if ($productName == "" || $catID == "" || $brandID == "" || $body == "" || $price == "" || $type == "") {
            $msg = "<span class='error'> Fields must be required.</span>";
            return $msg;
        } else {
            if (!empty($file_name)) {
                if ($file_size > 1048567) {
                    echo "<span class='error'>Image Size should be less then 1MB!</span>";
                } elseif (in_array($file_ext, $permited) === false) {
                    echo "<span class='error'>You can upload only:- " . implode(', ', $permited) . "</span>";
                } else {
                    move_uploaded_file($file_temp, $uploaded_image);
                    $query = " UPDATE table_product 
                            SET 
                            productName ='$productName',
                            catID ='$catID',
                            brandID ='$brandID',
                            body ='$body',
                            price ='$price',
                            image ='$uploaded_image',
                            type ='$type'
                            WHERE productId ='$id' ";
                    $updated_rows = $this->db->update($query);
                    if ($updated_rows) {
                        $msg = "<span class='success'>Product Updated Successfully.</span>";
                        return $msg;
                    } else {
                        $msg = "<span class='error'>Product Not Updated !</span>";
                        return $msg;
                    }
                }
            } else {
                $query = " UPDATE table_product SET 
                            productName ='$productName',
                            catID ='$catID',
                            brandID ='$brandID',
                            body ='$body',
                            price ='$price',
                            type ='$type'
                            WHERE productId ='$id' ";
                $updated_rows = $this->db->update($query);
                if ($updated_rows) {
                    $msg = "<span class='success'>Product Updated Successfully.</span>";
                    return $msg;
                } else {
                    $msg = "<span class='error'>Product Not Updated !</span>";
                    return $msg;
                }
            }

        }
    }

    public function delProductByID($id)
    {
        $query = "SELECT * FROM table_product WHERE productId='$id' ";
        $getData = $this->db->select($query);
        if ($getData) {
            while ($delImg = $getData->fetch_assoc()) {
                $dellink = $delImg['image'];
                unlink($dellink);
            }
        }
        $delquery = "DELETE  FROM table_product WHERE productId ='$id' ";
        $deldata = $this->db->delete($delquery);
        if ($deldata) {
            $msg = "<span class='success'>Product Deleted Successfully.</span>";
            return $msg;
        } else {
            $msg = "<span class='error'>Product Not Delete !</span>";
            return $msg;
        }
    }

    public function getFeautredPro()
    {
        $query = "SELECT * FROM table_product WHERE type='0' ORDER BY productId DESC LIMIT 4";
        $result = $this->db->select($query);
        return $result;
    }

    public function getNewPro()
    {
        $query = "SELECT * FROM table_product  ORDER BY productId DESC LIMIT 4";
        $result = $this->db->select($query);
        return $result;
    }

    public function getSinglePro($id)
    {
        $query = "SELECT table_product.*, table_category.catName, table_brand.brandName
       FROM table_product
       INNER JOIN table_category
        ON table_product.catID = table_category.catID
        INNER JOIN table_brand
        ON table_product.brandID = table_brand.brandID
        AND table_product.productId ='$id' ";
        $result = $this->db->select($query);
        return $result;
    }

    public function latestFromIphone()
    {
        $query = "SELECT * FROM table_product WHERE brandID='8' ORDER BY productId DESC LIMIT 1";
        $result = $this->db->select($query);
        return $result;
    }

    public function latestFromSamsung()
    {
        $query = "SELECT * FROM table_product WHERE brandID='7' ORDER BY productId DESC LIMIT 1";
        $result = $this->db->select($query);
        return $result;
    }

    public function latestFromCanon()
    {
        $query = "SELECT * FROM table_product WHERE brandID='9' ORDER BY productId DESC LIMIT 1";
        $result = $this->db->select($query);
        return $result;
    }

    public function latestFromAcer()
    {
        $query = "SELECT * FROM table_product WHERE brandID='4' ORDER BY productId DESC LIMIT 1";
        $result = $this->db->select($query);
        return $result;
    }

    public function proByCat($id)
    {
        $catID = mysqli_real_escape_string($this->db->link, $id);
        $query = "SELECT * FROM table_product WHERE catID ='$catID' ";
        $result = $this->db->select($query);
        return $result;
    }

    public function insertCompareData($customerid, $productId)
    {
        $customerid = mysqli_real_escape_string($this->db->link, $customerid);
        $productId = mysqli_real_escape_string($this->db->link, $productId);
        $comQuery = "SELECT * FROM table_compare WHERE   productId = '$productId'";
        $check = $this->db->select($comQuery);
        if ($check) {
            $msg = "<span class='error'>Already Added.</span>";
            return $msg;
        }

        $query = "SELECT * FROM table_product WHERE   productId = '$productId'";
        $result = $this->db->select($query)->fetch_assoc();
        if ($result) {
            $productId = $result['productId'];
            $productName = $result['productName'];
            $price = $result['price'];
            $image = $result['image'];
            $query = "INSERT INTO table_compare (customerid,productId,productName,price,image) 
                VALUES('$customerid','$productId','$productName','$price','$image')";
            $inserted_rows = $this->db->insert($query);
            if ($inserted_rows) {
                $msg = "<span class='success'>Added to Compare.</span>";
                return $msg;
            } else {
                $msg = "<span class='error'> Not Added !</span>";
                return $msg;
            }

        }
    }

    public function getCompareProduct($customerid)
    {
        $query = "SELECT * FROM table_compare WHERE   customerid = '$customerid' ORDER BY id DESC ";
        $result = $this->db->select($query);
        return $result;
    }

    public function delCompareData($customerid)
    {
        $query = "DELETE FROM table_compare WHERE customerid='$customerid' ";
        $delData = $this->db->delete($query);

    }

    public function saveWishlistData($customerid, $productId)
    {
        $comQuery = "SELECT * FROM table_wishlist WHERE   productId = '$productId'";
        $check = $this->db->select($comQuery);
        if ($check) {
            $msg = "<span class='error'>Already Added to Wishlist Page.</span>";
            return $msg;
        }

        $query = "SELECT * FROM table_product WHERE   productId = '$productId'";
        $result = $this->db->select($query)->fetch_assoc();
        if ($result) {
            $productId = $result['productId'];
            $productName = $result['productName'];
            $price = $result['price'];
            $image = $result['image'];
            $query = "INSERT INTO table_wishlist (customerid,productId,productName,price,image) 
                VALUES('$customerid','$productId','$productName','$price','$image')";
            $inserted_rows = $this->db->insert($query);
            if ($inserted_rows) {
                $msg = "<span class='success'>Success..!Added to Wishlist Page.</span>";
                return $msg;
            } else {
                $msg = "<span class='error'> Not Added !</span>";
                return $msg;
            }
        }

    }
    public function getWishlistProduct($customerid){
        $query = "SELECT * FROM table_wishlist WHERE   customerid = '$customerid' ORDER BY id DESC ";
        $result = $this->db->select($query);
        return $result;
    }
    public function delProBywishlistId($customerid, $productId){
        $query = "DELETE FROM table_wishlist WHERE customerid ='$customerid' AND productId='$productId'";
        $wishlistDeleted = $this->db->delete($query);
        if ($wishlistDeleted){
            echo "<script> window.location= 'wishlist.php' ; </script>";
        }else{
            $msg = "<span class='error'>Opps...! Wishlist product not Delete ... </span>";
            return $msg;
        }
    }
















}


?>
