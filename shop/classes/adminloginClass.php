<?php
    $filePath = realpath(dirname(__FILE__));
    include ($filePath.'/../library/session.php');
    Session::checkLogin();

    include_once ($filePath.'/../library/database.php');
    include_once ($filePath.'/../helpers/format.php');
?>
<?php
class adminLogin{
    private $db;
    private $fm;
    public function __construct()
    {
        $this->db = new Database();
        $this->fm = new Format();
    }
    public function adminLogin($adminUser , $adminPass){
        $adminUser = $this->fm->validation($adminUser);
        $adminPass = $this->fm->validation($adminPass);

        $adminUser = mysqli_real_escape_string($this->db->link,$adminUser);
        $adminPass = mysqli_real_escape_string($this->db->link,$adminPass);
        if (empty($adminUser)||empty($adminPass)){
            $loginMSG = "Username or Password must be required ";
            return $loginMSG;
        }else{
            $query = "SELECT * FROM admin_tbl WHERE adminUser = '$adminUser' AND adminPass ='$adminPass'";
            $result = $this->db->select($query);
            if ($result != false){
                $value = $result->fetch_assoc();
                Session::set("adminLogin", true);
                Session::set("adminID",$value['adminID']);
                Session::set("adminName",$value['adminName']);
                Session::set("adminUser",$value['adminUser']);
                header("Location:dashboard.php");
            }else{
                $loginMSG ="Opps..! Username or Password not match.";
                return $loginMSG;
            }
        }

    }
}
?>