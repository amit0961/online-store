<?php
$filePath = realpath(dirname(__FILE__));
include_once ($filePath.'/../library/database.php');
include_once ($filePath.'/../helpers/format.php');
?>

<?php
class Category
{
    private $db;
    private $fm;
    public function __construct()
    {
        $this->db = new Database();
        $this->fm = new Format();
    }
    public function catInsert($catName){
        $catName = $this->fm->validation($catName);
        $catName = mysqli_real_escape_string($this->db->link,$catName);
        if (empty($catName)){
            $msg = "<span class='error'>Category field must be required </span>";
            return $msg;
        }else{
            $query = "INSERT INTO table_category(catName) VALUES ('$catName')";
            $catInsert = $this->db->insert($query);
            if ($catInsert){
                $msg = "<span class='success'>Category Inserted Successfully... </span>";
                return $msg;
            }else{
                $msg = "<span class='error'>Opps...! Category not Inserted ... </span>";
                return $msg;
            }
        }
    }
    public function getallCategory(){
        $query = "SELECT * FROM table_category ORDER BY catID DESC ";
        $result = $this->db->select($query);
        return $result;
    }
    public function getCategoryByID($id){
        $query = "SELECT * FROM table_category WHERE catID ='$id' ";
        $result = $this->db->select($query);
        return $result;
    }
    public function catUpdate($catName, $id){
        $catName = $this->fm->validation($catName);
        $catName = mysqli_real_escape_string($this->db->link,$catName);
        $id = mysqli_real_escape_string($this->db->link,$id);
        if (empty($catName)){
            $msg = "<span class='error'>Category field must be required </span>";
            return $msg;
        }else{
            $query = "UPDATE table_category SET catName ='$catName' WHERE catID='$id'";
            $catUpdated = $this->db->update($query);
            if ($catUpdated){
                $msg = "<span class='success'>Category Updated Successfully... </span>";
                return $msg;
            }else{
                $msg = "<span class='error'>Opps...! Category not Updated ... </span>";
                return $msg;
            }
        }
    }
    public function delcategoryByID($id){
        $query = "DELETE FROM table_category WHERE catID ='$id'";
        $catDeleted = $this->db->delete($query);
        if ($catDeleted){
            $msg = "<span class='success'>Category Deleted Successfully... </span>";
            return $msg;
        }else{
            $msg = "<span class='error'>Opps...! Category not Delete ... </span>";
            return $msg;
        }
    }

}















