<?php include 'inc/header.php'; ?>
<?php
if (!isset($_GET['proId']) || $_GET['proId'] == null) {
    echo "<script>window.location='404.php'; </script>";
} else {
    $id = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['proId']);
}
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {
    $quantity = $_POST['quantity'];
    $addCart = $cart->addToCart($quantity, $id);
}
?>
<?php
$customerid = Session::get("customerid");
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['compare'])) {
    $productId = $_POST['productId'];
    $insertCom = $product->insertCompareData($customerid, $productId);
}
?>
<?php
    $customerid = Session::get("customerid");
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['wishlist'])) {
        $productId = $_POST['productId'];
        $saveWishlist = $product->saveWishlistData($customerid, $productId);
    }
?>
    <style>
        .mybutton{
            width: 100px;
            float: left;
            margin-right: 40px;
        }
    </style>
    <div class="main">
        <div class="content">
            <div class="section group">
                <div class="cont-desc span_1_of_2">
                    <?php
                    $getPro = $product->getSinglePro($id);
                    if ($getPro) {
                        while ($result = $getPro->fetch_assoc()) {
                            ?>
                            <div class="grid images_3_of_2">
                                <img src="admin/<?php echo $result['image']; ?>" alt=""/>
                            </div>
                            <div class="desc span_3_of_2">
                                <h2><?php echo $result['productName']; ?></h2>
                                <div class="price">
                                    <p>Price: <span>$<?php echo $result['price']; ?></span></p>
                                    <p>Category: <span><?php echo $result['catName']; ?></span></p>
                                    <p>Brand:<span><?php echo $result['brandName']; ?></span></p>
                                </div>
                                <div class="add-cart">
                                    <form action="" method="post">
                                        <input type="number" class="buyfield" name="quantity" value="1"/>
                                        <input type="submit" class="buysubmit" name="submit" value="Buy Now"/>
                                    </form>
                                </div>
                                <span style="font-size: 18px; color: red;">
                                <?php
                                if (isset($addCart)) {
                                    echo $addCart;
                                }
                                ?>
                            </span>
                                <?php
                                    if (isset($insertCom)) {
                                        echo $insertCom;
                                    }
                                    if (isset($saveWishlist)) {
                                        echo $saveWishlist;
                                    }

                                ?>
                                <?php
                                $login = Session::get('customerLogin');
                                if ($login == true) { ?>
                                    <div class="add-cart">
                                        <div class="mybutton">
                                            <form action="" method="post">
                                                <input type="hidden" class="buyfield" name="productId"
                                                       value="<?php echo $result['productId']; ?>"/>
                                                <input type="submit" class="buysubmit" name="compare"
                                                       value="Add To Compare"/>
                                            </form>
                                        </div>
                                        <div class="mybutton">
                                            <form action="" method="post">
                                                <input type="hidden" class="buyfield" name="productId"
                                                       value="<?php echo $result['productId']; ?>"/>
                                                <input type="submit" class="buysubmit" name="wishlist"
                                                       value="Add to Wishlist"/>
                                            </form>
                                        </div>

                                    </div>
                                <?php } ?>
                            </div>
                            <div class="product-desc">
                                <h2>Product Details</h2>
                                <p><?php echo $result['body']; ?></p>
                            </div>
                        <?php }
                    } ?>
                </div>

                <div class="rightsidebar span_3_of_1">
                    <h2>CATEGORIES</h2>
                    <ul>
                        <?php
                        $getCat = $cat->getallCategory();
                        if ($getCat) {
                            while ($result = $getCat->fetch_assoc()) {
                                ?>
                                <li>
                                    <a href="productbycat.php?catID=<?php echo $result['catID']; ?>"><?php echo $result['catName']; ?></a>
                                </li>
                            <?php }
                        }
                        ?>

                    </ul>

                </div>
            </div>
        </div>
    </div>
<?php include 'inc/footer.php'; ?>