<?php include 'inc/header.php'; ?>
<?php
$login = Session::get("customerLogin");
if ($login == false) {
    header("Location:login.php");
}
?>

<?php
    if (isset($_GET['orderid'])&& $_GET['orderid']=='order'){
        $customerid = Session::get("customerid");
        $insertOrder = $cart->orderProduct($customerid);
        $delData =$cart->delCustomerCart();
        header("Location:success.php");
    }
?>
    <style>
        .tbltwo{
            width: 50%;
            float: right;
        }
        .order{
            padding-bottom: 30px;
        }
        .order a{
            width: 200px;
            margin: 20px auto 0;
            text-align: center;
            padding: 5px;
            font-size: 30px;
            display: block;
            background: #ff0000;
            color: #fff;
            border-radius: 3px;
        }

    </style>
    <div class="main">
        <div class="content">
            <div class="section-group">
                <div class="division">
                    <form action="" method="post">
                        <table class="tblone">
                        <tr>
                            <td >
                                <h2>Check Your Cart</h2>
                            </td>
                        </tr>
                        <tr>
                            <th>No</th>
                            <th >Product </th>
                            <th >Price</th>
                            <th >Quantity</th>
                            <th >Total Price</th>
                        </tr>
                        <?php
                        $getPro = $cart->getCartProduct();
                        if ($getPro) {
                            $i =0;
                            $sum = 0;
                            $qty =0 ;
                            while ($result = $getPro->fetch_assoc()) {
                                $i ++;
                                ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo $result['productName'];?></td>
                                    <td><?php echo $result['price'];?></td>
                                    <td><?php echo $result['quantity'];?></td>

                                    <td>$
                                        <?php
                                        $total = $result['price'] * $result['quantity'];
                                        echo $total;?>
                                    </td>
                                </tr>
                                <?php
                                $qty = $qty + $result['quantity'] ;
                                $sum = $sum + $total;
                                Session::set("qty", $qty);
                                Session::set("sum", $sum);
                                ?>
                            <?php }} ?>

                    </table>
                    </form>
                    <table class="tbltwo">
                        <tr>
                            <th>Sub Total :</th>
                            <td>$ <?php echo $sum ;?> </td>
                        </tr>
                        <tr>
                            <th>VAT (10%) :</th>
                            <td> $ <?php
                                $vat = $sum * 0.1;
                                echo $vat;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Grand Total :</th>
                            <td> $
                                <?php
                                $vat = $sum * 0.1;
                                $gtotal = $sum + $vat;
                                echo $gtotal ;
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="division">
                    <?php
                    $id = Session::get('customerid');
                    $getData = $customer->getCustomer($id);
                    if ($getData) {
                        while ($result = $getData->fetch_assoc()) {
                            ?>
                            <form action="" method="post">
                                <table class="tblone">
                                    <tr>
                                        <td colspan="3">
                                            <h2>Shipping Details Here</h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >Name</td>
                                        <td ><?php echo $result['name']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td><?php echo $result['address']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>City</td>
                                        <td> <?php echo $result['city']; ?> </td>
                                    </tr>
                                    <tr>
                                        <td>Country</td>
                                        <td><?php echo $result['country']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Zip Code</td>
                                        <td><?php echo $result['zip']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Phone</td>
                                        <td><?php echo $result['phone']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><?php echo $result['email']; ?></td>

                                    </tr>
                                    <tr >
                                        <td></td>
                                        <td><input type="submit" name="submit" value="Update"></td>
                                    </tr>
                                </table>
                            </form>

                        <?php }
                    } ?>
                </div>
            </div>
        </div>
        <div class="order">
            <a href="?orderid=order">Order-Now</a>
        </div>
    </div>
<?php include 'inc/footer.php'; ?>