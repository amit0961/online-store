<?php include 'inc/header.php'; ?>
<?php
$login = Session::get("customerLogin");
if ($login == false) {
    header("Location:login.php");
}
?>
<?php
    if (isset($_GET['confirmid'])){
        $id = $_GET['confirmid'];
        $price = $_GET['price'];
        $confirmid = $cart->productShiftConfirm($id, $price);
    }
?>
    <div class="main">
        <div class="content">
            <div class="section-group">
                <div class="notfound">
                    <h2>Your Order Details...</h2>
                    <table class="tblone">
                        <tr>
                            <th>No.</th>
                            <th>Product Name</th>
                            <th>Image</th>
                            <th>Quantity</th>
                            <th> Price</th>
                            <th> Order Date</th>
                            <th> Status</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        $customerid = Session::get("customerid");
                        $getOrder = $cart->getOrderProduct($customerid);
                        if ($getOrder) {
                            $i = 0;
                            while ($result = $getOrder->fetch_assoc()) {
                                $i++;
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $result['productName']; ?></td>
                                    <td><img src="admin/<?php echo $result['image']; ?>" alt=""/></td>
                                    <td><?php echo $result['quantity']; ?></td>
                                    <td>$
                                        <?php
                                        $total = $result['price'];
                                        echo $total; ?>
                                    </td>
                                    <td><?php echo $fm->formatDate($result['date']); ?></td>
                                    <td><?php
                                        if ($result['status'] == '0') {
                                            echo "Pending";
                                        } elseif ($result['status'] == '1') {
                                            echo "Shifted";
                                        } else {
                                            echo "DONE";
                                        } ?></td>
                                    <?php
                                    if ($result['status'] == '1') { ?>
                                        <td><a href="?confirmid=<?php echo $customerid ;?>&price=<?php echo $total ;?>">Confirm</a></td>
                                    <?php } else if($result['status'] == '2') { ?>
                                        <td>OK</td>
                                    <?php }else{ ?>
                                        <td>N/A</td>
                                  <?php } ?>
                                </tr>
                            <?php }
                        } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php include 'inc/footer.php'; ?>