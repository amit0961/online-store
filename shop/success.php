<?php include 'inc/header.php'; ?>
<?php
$login = Session::get("customerLogin");
if ($login == false) {
    header("Location:login.php");
}
?>
    <style>
        .psuccess{ width: 500px; min-height: 200px;text-align: center;border:1px solid #ddd; margin: 0 auto; padding: 20px;}
        .psuccess h2{ border-bottom: 1px solid #ddd; margin-bottom: 40px; padding-bottom:10px;}
        .psuccess p{
            text-align:center;
            margin-bottom: 5px;
            padding : 5px;
            font-size: 18px;
        }
        .psuccess p span{
           color:red;
            font-weight: bold;
        }
    </style>
    <div class="main">
        <div class="content">
            <div class="section-group">
                <div class="psuccess">
                    <h2>Success</h2>
                    <?php
                        $customerid =Session::get("customerid");
                        $amount = $cart ->payableamount($customerid);
                        if ($amount){
                            $sum = 0;
                            while($result = $amount->fetch_assoc()){
                                $price = $result['price'];
                                $sum = $sum+$price;
                            }
                        }
                    ?>
                    <p>Total Payable Amount(including vat=10%) : <span>
                            $<?php
                        $vat = $sum * 0.1;
                        $total = $sum + $vat ;
                        echo $total;
                        ?>
                        </span>
                        </p>
                    <p>Thanks for purchasing. Receive your order successfully. <br><br> We will contact with you as soon as possible with delivery details.
                        <br> Here your order details...  <a href="orderDetails.php">visit here</a></p>
                </div>
            </div>
        </div>
    </div>
<?php include 'inc/footer.php'; ?>